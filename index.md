# Skoleni GIT

Nabizim skoleni GITu pro zacatecniky i pokrocile uzivatele.

Cena kazdeho kurzu je 800 CZK. Pro studenty nabizim slevu 30%, cena je tedy 560 CZK.

Pro oba kurzy najednou je celkova cena 1400 CZK (pro studenty 980 CZK), pro vice osob soucasne je mozne dohodnout slevu.

Pokud mate o kurzy zajem nebo mate jakykoliv dotaz, nevahejte me kontaktovat ohledne terminu na email: <ondrej@ondrejsika.com>.

Pokud chcete dostavit novinky o skupinovych skolenich a seminarich, muzete se prihlasit k odberu newsletteru na <http://url.os1.cz/skoleni-git-newsletter/>.



## Zaklady v GITu

* co to vlatne verzovani je
* instalace na Linux, Windows i Mac
* vytvoreni repozitare, vytvoreni revize
* vzdalene repozitare (push, pull)
* stash
* vetve - zaklady
* gitk a jine GUI
* github, bitbucket


## GIT pro pokrocile

* pokrocila prace s vetvemi
* pokrocila prace se vzdalenymi repozitaremi
* jak provozovat vlastni vzdaleny repozitar
* hooks
* submodules (vnorene repozitare)
* tig, gitlab


### Lektor Ondrej Sika

* studuji na MFF UK
* pracuji na projektech jako je Slush Pool
* podnikam na internetu, nekere moje projekty: PHPHost, Neomail, MyBitcoinPayments

### Reference

* "Školení s panem Sikou bylo naprosto v pořádku. Odnesl jsem si z něj přesně to, co jsem potřeboval. S panem Sikou jsme nadále v kontaktu pro případné dotazy nebo další školení." - Lukas Blazicek

---

Stranku si muzete stahnout v PDF na <http://skoleni-git.cz/skoleni_git.pdf>

2015 (c) [Ondrej Sika](http://ondrejsika.com)

